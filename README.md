# data-engineer-tasks

Tasks for candidates on the Data Engineer (Python) role

## Getting started

This repository contains tasks to perform by candidates on the Data Engineer (Python) role.

Please find below the [Tasks](#Task) and [Submitting process and requirements](#Submit your task-response). All required input data are located in the main branch.

Ask any questions related to Task or Submittion by creating an [**Issue**](https://gitlab.com/p-a-system-team/data-engineer-june-2022/-/issues)


## Submit your tasks-response

1. Clone this repository
2. Create new branch named as __your-surname-name__. Please, not use nickname, accounts, etc. as brach name will be used to match with your CV.
3. Create separate folder per each task response named **Task_{i}**
4. Inside each task-folder locate your response, including code and other artifacts. Please, locate any text answer for a task in `README.md` of corresponding task-folder. Also `README.md` _must not be empty_, please put there meaningful description of commined artifacts.
5. Request access for a developer role to this project according to [gitlab documentation](https://docs.gitlab.com/ee/user/project/members/#request-access-to-a-project)
6. Push your brach to this repository

## Tasks

### Task 1

Please, implement components of a data acquisitin-ingestion pipeline following requirements and restrictions below.

1. The pipeline include two acynchronuos steps:
	1. data acquisition from data source
	2. data ingestion to relation database
2. The pipeline should on its Step 1
    1. acquire regularly data source
    2. get daily historical data of economics indicator
    3. The deep of the history should be configured
    4. The API of the data source shoud be investigated and any appropriate approach and tools should be suggested(used) to get data with a _free access_ level.
3. The output transport format for the Step 1 should be _file_. The output data format should be native format of data source.
4. The file should be placed into configured destination on a local file system.
5. The acquired data then on the Step 2 will be read per file and ingested into data storage by existen module. This module accepts an adapter function per each supported file type with a following signature and data types restrictions:

	```python

	def read_xxx_file(file_path: Union[Path, str]) -> List[TimeSeries]:
        ...
	
	class DataSourceFileReader:
		def __init__(self, data_source_type: str):
			...
			self.reader = read_xxx_file

	    def read_form_file(self, datasource_file: Union[Path, str]) -> List[TimeSeries]:
			series: List[TimeSeries] = []
			try:
				tmp_series: List[TimeSeries] = self.reader(datasource_file)
				# Assert that reader function produce pandas Series with DataTimeIndex
				if tmp_series:
					assert isinstance(tmp_series[0].series, pd.Series),
                                      f'{self.reader = } must return a pandas.Series object'
					assert isinstance(tmp_series[0].series.index, DatetimeIndex),
                                      f'{self.reader = } must return a pandas.Series with a pandas.DatetimeIndex'
    ...
	from pandas import Series
	
	@dataclass
	class TimeSeries:
		series: Series
		symbol: str
		description: str
	```

5. The adapters results then should be written into sqlite database file with a dedicated table per time series. Please, consider for the task the simpliest db writing stub implementation just to demonstrate end-to-end functionality.

**Expected outputs**:
1. Source code of implemented components. Please, consider acquisition of [S&P 500 Historical Data][1.1] for the task purpose
2. Executables which could be run from CLI just after chechout the branch
3. Pipeline steps outputs:
	1. acquired file
	2. sqlite db file
	
[1.1]:https://www.investing.com/indices/us-spx-500-historical-data

### Task 2

Please, implement custom [scikit-learn][2.1] API compartible transformer to be used as part of scikit-learn [`ColumnTransformer`][2.3] and [`Pipeline`][2.4].

1. The raw data is a list of prices time series in csv format. Test data are in [MB-STS-0096-Mid.csv][2.0.1] and [SO-STE-0006-Avg.csv][2.0.2]
2. The dataset preparation include feature generation step. The scikit learn framework is used to configure and perform preprocessing.
3. As one of the features we are going to produce time series decomposition with a help of [statsmodels][2.2] library
4. The custom transformer should be compartible with [`Pipeline`][2.4] interfaces including ability to be part of cross validation.

**Expected outputs**
1. Custom Transformer class performing fitting [STL][2.5] model
2. Unit tests including:
	- reading input series into `pd.DataFrame` object
	- composing ColumnTransformer and Pipeline
	- transforming each of time series (without/with [TimeSeriesSplit][2.6] Cross Validation)
	- exporting transformed data as one DataFrame(s) into .csv. Columns name format should be **[<symbol>_STL_<decomposition_component_name>]**
3. Feature generatin step outputs:
	1. .csv with transformed time series


[2.0.1]:../data/MB-STS-0096-Mid.csv
[2.0.2]:../data/SO-STE-0006-Avg.csv
[2.1]:https://scikit-learn.org/stable/install.html
[2.2]:https://www.statsmodels.org/stable/install.html
[2.3]:https://scikit-learn.org/stable/modules/generated/sklearn.compose.ColumnTransformer.html#sklearn.compose.ColumnTransformer
[2.4]:https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html#sklearn.pipeline.Pipeline
[2.5]:https://www.statsmodels.org/stable/generated/statsmodels.tsa.seasonal.STL.html#statsmodels.tsa.seasonal.STL
[2.6]:https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.TimeSeriesSplit.html#sklearn.model_selection.TimeSeriesSplit.split


## Contributing
This repository is suppoused to be contributed by candidates on the team role with responses on suggested tasks.
Making contribution to this repository each candidates acknowledge that his contributions are subject of parent repository license stated in License section below.

## License
MIT License

Copyright (c) 2022 P-A-Systems

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

